# Angular app Twitter Sentiments
Angular App that streams the actual sentiments of the differents hashtags

## Compile the Application and Build the image
```
$ npm install
$ bower install
$ grunt --force #Builds the Application Distribution
$ docker build -t dataspartan/twitter-angular .
...
```