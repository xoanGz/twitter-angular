#!/bin/sh

#define parameters which are passed in.
#SOCKET_END_POINT=$1
#REST_END_POINT=$2
#hashtags=$3

#define the template.
cat  << EOF
(function (window) {
  window.__env = window.__env || {};
  // Socket End Point
  window.__env.socketEndPoint = '$SOCKET_END_POINT';
  // Rest End Point
  window.__env.restEndPoint = '$REST_END_POINT';
  // HashTAgs
  window.__env.hashtags = '$hashtags';
  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = true;
}(this));
EOF