'use strict';

/**
 * @ngdoc function
 * @name twitterSentimentApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the twitterSentimentApp
 */
angular.module('twitterSentimentApp')
  .controller('MainCtrl', ['$scope', '$http', '__env', function ($scope, $http, __env) {
    $scope.hashTags = __env.hashtags.split(',');
    $scope.selectedHashTags = [];

$scope.selectHashTag = function ($item, $model) {
      var plainHash = $item.replace('#', '').toLowerCase();
      $http.get(__env.restEndPoint+"twitter/sentiment/"+plainHash)
        .then(function(response) {
          var hashtagJson = {
            key: $item,
            data: response.data
          };
          
          $scope.selectedHashTags.push(hashtagJson);
          var index = $scope.hashTags.indexOf($item);
          if (index > -1) {
            $scope.hashTags.splice(index, 1);
          }
        });
    }

  }]);
