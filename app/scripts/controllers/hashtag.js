'use strict';

/**
 * @ngdoc function
 * @name twitterSentimentApp.controller:HashtagCtrl
 * @description
 * # HashtagCtrl
 * Controller of the twitterSentimentApp
 */
angular.module('twitterSentimentApp')
  .controller('HashtagCtrl', ['$scope', 'socket', function ($scope, socket) {
    
    var hashtag = $scope.hashtag;
    $scope.id = hashtag.key.replace('#', '').toLowerCase();
    $scope.hashtagname = hashtag.key;
    $scope.hashtagscore = hashtag.data.value.emea;
    $scope.hashtagts = hashtag.data.value.ts;

    $scope.getRandomSentiment = function(){
      return Math.floor((Math.random()*10));
    };

    socket.on('twitter-socket', function (msg) {
      var response = JSON.parse(msg);
      if ($scope.id === response.key) {
        $scope.hashtagscore = response.value.emea;
        $scope.hashtagts = response.value.ts;
      }
      console.log($scope.id + " " + msg);
    });
  }]);
