'use strict';

/**
 * @ngdoc service
 * @name twitterSentimentApp.socket
 * @description
 * # socket
 * Factory in the twitterSentimentApp.
 */
angular.module('twitterSentimentApp')
  .factory('socket', ['$rootScope', '__env', function ($rootScope, __env) {
    var socket = io.connect(__env.socketEndPoint);
    return {
      on: function (eventName, callback) {
        socket.on(eventName, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            callback.apply(socket, args);
          });
        });
      }
    };
  }]);
