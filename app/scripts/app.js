(function () {
  'use strict';
  /**
   * @ngdoc overview
   * @name twitterSentimentApp
   * @description
   * # twitterSentimentApp
   *
   * Main module of the application.
   */
  angular
    .module('twitterSentimentApp', [
      'ngResource',
      'ngSanitize',
      'ngMaterial',
      'ui.select',
      'jkAngularRatingStars'
    ]);

  var env = {};

  // Import variables if present (from env.js)
  if (window) {
    Object.assign(env, window.__env);
  }

  angular
    .module('twitterSentimentApp').constant('__env', env);

})();