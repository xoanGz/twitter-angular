(function (window) {
  window.__env = window.__env || {};
  // Socket End Point
  window.__env.socketEndPoint = 'http://localhost:3011/';
  // Rest End Point
  window.__env.restEndPoint = 'http://localhost:3010/';
  // HashTAgs
  window.__env.hashtags = '#NBAAwards,#TheBachelorette,#TeenMomOG,#BasketballWives';
  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = true;
}(this));