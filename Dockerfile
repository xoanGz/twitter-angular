FROM httpd:2.4
COPY /dist /usr/local/apache2/htdocs/
RUN mkdir /bash
COPY generate.sh /bash
RUN mkdir /usr/local/apache2/htdocs/scripts/env
EXPOSE 80
