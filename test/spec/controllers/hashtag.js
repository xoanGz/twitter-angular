'use strict';

describe('Controller: HashtagCtrl', function () {

  // load the controller's module
  beforeEach(module('twitterSentimentApp'));

  var HashtagCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HashtagCtrl = $controller('HashtagCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(HashtagCtrl.awesomeThings.length).toBe(3);
  });
});
